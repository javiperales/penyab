<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cathegory;
use App\Order;
use \Carbon\Carbon;
use \App\User;
use Session;
class BasketController extends Controller
{
   public function __construct()
   {
    $this->middleware('auth');

}
public function index(Request $request)
{
    $total=0;


    $basket = $request->session()->get('basket');
    if(! $basket){$basket=[];}
    $total=0;
    foreach($basket as $precioPedido){
        $total+=$precioPedido->importe;
    }

    return view('basket.index', ['products'=>$basket],['total'=>$total]);


    return $basket;
}


public function flush(Request $request)
{

    $request->session()->forget('basket');
    return back();
}

public function addProduct(Request $request ,$id)
{


    $products = Product::findOrFail($id);

    $basket =$request->session()->get('basket');

    if($basket ==null){
        $basket = array();
    }

    $position=-1;
    foreach ($basket as $key => $item) {
        if($item->id == $id){
            $position = $key;
            break;
        }
    }



    if ($position == -1) {
        $products->cantidad =1;
        $products->importe=$products->price;
        $request->session()->push('basket', $products);

    } else {

        $basket[$position]->cantidad++;
        $basket[$position]->importe=$products->price*$basket[$position]->cantidad;

    }
    $total=0;



    return redirect('/basket');

}

    public function delete(Request $request,$id){//para borrar un elemento de Sesion
        $products=Product::findOrFail($id);
        $basket=$request->session()->get('basket');

        foreach ($basket as $key => $productsSesion){
            /*userSesion son los usuarios que actualmente estan la group*/
            if ($products->id==$basket[$key]->id){
                if ($productsSesion->cantidad <=1){
                    $request->session()->forget('basket.' .$key);
                }else{
                    $productsSesion->cantidad--;
                }
                return back();
            }
        }
        return back();
    }//fin delete

    public function saveBasket(){
        $productsSesion;

        $order = New Order;
        $order->user_id=$user->id;
        $order->date =$date;
        $order->save();

        foreach ($productsSesion as $products) {
            $order->products()->attach($products->id,['price'=>$price, 'quantity'=>$quantity,]);
        }

    }//falta vaciar cesta



    public function store(Request $request)
    {
        $order = New Order;
        $basket = $request->session()->get('basket');
        $order->paid = 0;
        $order->date = Carbon::now();
        $order->user_id = Auth()->user()->id;

        $order->save();

        foreach ($basket as $products) {
          $order->products()->attach($products->id,
            [
              'price' => $products->price,
              'quantity' => $products->cantidad,
          ]);
      }

    $orderController = new OrderController;
    $orderController->mail($order->id);

      return redirect("/basket/flush");
  }

  public static function total()
  {
    $basket= Session::get('basket');
        if ($basket == null ) {
            $basket = array();
            return '';
        }
        $cantidadTotal = 0;
        $precioTotal=0;

        foreach ($basket as $key ) {
            $numProducts=$key->cantidad;
            $cantidadTotal+=$numProducts;
            $precioTotal += $key->price*$key->cantidad;
        }
        return $cantidadTotal." - ".$precioTotal."€";
  }


}
