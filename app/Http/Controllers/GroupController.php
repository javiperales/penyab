<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;
class GroupController extends Controller
{
    public function index(Request $request)
    {
        $group = $request->session()->get('group');
        if(! $group){$group=[];}
        return view('group.index', ['users'=>$group]);
        //$request->session()->forget('group');

       return $group;
    }

    public function flush(Request $request)
    {

        $request->session()->forget('group');
        return back();
    }

    public function addUser(Request $request ,$id)
    {
        $user = User::findOrFail($id);

        $group =$request->session()->get('group');

        if($group ==null){
            $group = array();
        }

        $position=-1;
        foreach ($group as $key => $item) {
            if($item->id == $id){
                $position = $key;
                break;
            }
        }

        if($position == -1){
            $request->session()->push('group', $user);

        }else{
            $group[$position]->cantidad++;
        }
        return redirect('/group');
        //buscar usuario
        //comprobar si esta en  grupo de sesion
        //si no esta, añadirlo
    }

    public function delete(Request $request,$id){//para borrar un elemento de Sesion
        $user=User::findOrFail($id);
        $group=$request->session()->get('group');

        foreach ($group as $key => $userSesion){
            /*userSesion son los usuarios que actualmente estan la group*/
            if ($user->id==$group[$key]->id){
                if ($userSesion->cantidad <=1){
                    $request->session()->forget('group.' .$key);
                }else{
                    $userSesion->cantidad--;
                }
                return back();
            }
        }
        return back();
    }//fin delete
}
