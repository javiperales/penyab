<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    public function users()
    {
       return $this->hasMany(User::class);
       //belongsTo
       //hasMany
       //hasOne
       //belongsToMany(en los dos)
       //ejemplo N:M productos con categorias
       //la tabla de la relacion se debe llamar obligatoriamente cathegory_product
       //alfabeticamente, cathegory antes que product
       //product_cathegory no funcionaria
    }
}

