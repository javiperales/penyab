<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{

    protected $fillable =[
        'paid', 'date', 'user_id'
    ];

    protected $dates =[
       'date'
   ];
   public function user()
   {
    return $this->belongsTo(User::class);
}


public function products()
{
    return $this->belongsToMany(Product::class)->withPivot('order_id','product_id','quantity','price');
}

    /*public function total()
    {
        $precioTotal=0;
        $products = DB::select('SELECT * FROM order_product WHERE order_id=' .$this->id);

        foreach ($products as $key => $product) {
            $precioTotal+=$product->price*$product->quantity;
     }
        //para sacar el precio total del producto
 }*/
    public function total($id){ // esto es el "paid" del store una vez confirmamos la compra.

    $order = Order::findOrFail($id);
    $total = 0;
        foreach ($order->products as $product) { // this se refiere al objeto que llama, osea a $order
            $total += $product->pivot->price * $product->pivot->quantity;

        }
        return $total;
    }

    public function pedidosProducto()
    {

        return $this->belongsToMany(Product::class)->withPivot('order_id','product_id','quantity','price');
    //para sacar los pedidos dentro de la orden
    }




}
