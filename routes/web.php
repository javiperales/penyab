<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use \Carbon\Carbon;
Route::get('/', function () {
    return view('welcome');
});

use App\Mail\Bienvenida;

// Route::get('users', 'UserController@index')->name('usuarios');
// Route::get('users/{id}', 'UserController@show');
// Route::get('users/{id}/edit', 'UserController@edit');
// Route::get('users/create', 'UserController@create');
// Route::put('users/{id}', 'UserController@update');
// Route::delete('users/{id}', 'UserController@destroy');
// Route::post('users', 'UserController@store');
// Route::get('users/create', 'UserController@create');

//Ruta de tipo resource: equivale a las 7 rutas REST
//Ruta especial antes que resource, si no "show...."
Route::get('users/especial', 'UserController@especial');
Route::get('users/{id}/edit2', 'UserController@edit2');
Route::resource('users', 'UserController');
Route::resource('cathegory', 'CathegoryController');
Route::resource('product', 'ProductController');
Route::resource('roles', 'RoleController');
//Route::resource('basket', 'BasketController');






////////////////////////////////////////////
//Ejemplos de rutas con funciones anónimas:
////////////////////////////////////////////

Route::get('usuarios/{id}', function ($id) {
 return "Detalle del usuario $id";
});


Route::get('usuarios/{id}', function ($id) {
 return "Detalle del usuario $id";
})->where('id', '[0-9]+');

Route::get('usuarios/{id}/{name?}', function ($id, $name=null) {
    if($name) {
        return "Detalle del usuario $id. El nombre es $name";
    } else {
        return "Detalle del usuario $id. Anónimo";
    }
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/****grupos***/

Route::get('/group', 'GroupController@index');
Route::get('/group/flush', 'GroupController@flush');
Route::delete('/group/{id}', 'GroupController@delete');
Route::get('/group/{id}', 'GroupController@addUser');

/*basket*/
Route::get('/basket', 'BasketController@index');
Route::get('/basket/flush', 'BasketController@flush');
Route::get('/basket/delete/{id}', 'BasketController@delete');
Route::get('/basket/{id}', 'BasketController@addProduct');
Route::post('basket', 'BasketController@store');

Route::get('/hoy', function(){
    $today =Carbon::today();
    return $today->fornat('d-m-Y');
} );

//order

Route::get('order', 'OrderController@index');
Route::get('/order/{id}/show', 'OrderController@show');
Route::get('/order/{id}/edit', 'OrderController@edit');
Route::put('order/{id}', 'OrderController@update');
Route::delete('order/{id}', 'OrderController@destroy');

Route::get('order/{id}/generatePDF','OrderController@generatePDF');

Route::get('/redirect', 'Auth\LoginController@redirectToProvider');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/redirectFacebook', 'Auth\LoginController@redirectToProviderFacebook');
Route::get('/callbackFacebook', 'Auth\LoginController@handleProviderCallbackFacebook');


Route::get('/redirectGitHub', 'Auth\LoginController@redirectToGitHub');
Route::get('/callbackGitHub', 'Auth\LoginController@handleGitHubCallback');


Route::get('orders/{id}/mail' ,  'OrderController@mail');



