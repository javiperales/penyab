@extends('layouts.app')

@section('title', 'Productos')

@section('content')
    <h1>Editar producto</h1>

    <form method="post" action="/product/{{ $product->id }}">
        {{ csrf_field() }}

        <input type="hidden" name="_method" value="PUT">

        <label>Nombre</label>
        <input type="text" name="name"
        value="{{ old('name') ? old('name') : $product->name }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>
         <label>precio</label>
        <input type="number" name="price"
        value="{{ old('price') ? old('price') : $product->price }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>

        <input type="submit" value="Guardar Cambios">
    </form>
@endsection
