@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">detalle del pedido {{$order->id}}<br>

          <div class="card-body">
           <table class="table">
            <tr>
              <td>id</td>
              <td>pagado</td>
              <td>fecha</td>
          </tr>

          <tr>
              <td>{{$order->id}}</td>
              <td>{{$order->paid}}</td>
              <td>{{$order->date}}</td>
          </tr>
          tr>
              <td>nombre</td>
              <td>precio</td>
              <td>cantidad</td>
          </tr>
           @foreach($products as $product)
          <tr>
            <td>{{$product->name}}</td>
            <td>{{$product->price}}</td>
            <td>{{$product->pivot->quantity}}</td>
        </tr>
        @endforeach
          <h3>productos de este pedido</h3>




    </table>
    <h5>precio total del pedido {{$total}}€</h5>
    <br>

    <a href="/order/{{$order->id}}/generatePDF" class="fa fa-file-pdf-o" style="color:green"> BAJAR PDF</a>
</div>

</div>
</div>
</div>
</div>
</div>

@endsection



