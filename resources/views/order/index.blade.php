@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">lista de pedidos<br>
          <div class="card-body">
           <table class="table">
            <tr>
              <td>id</td>
              <td>pagado</td>
              <td>fecha</td>
              <td>nombre </td>
              <td>pagar </td>
            </tr>
            @forelse($order as $orden)
            @can ('view', $orden)
            <tr>
              <td>{{$orden->id}}</td>
              @if($orden->paid==0)
              <td>no pagado</td>
              @else
              <td> pagado</td>
              @endif
              <td>{{$orden->date}}</td>
              <td>{{$orden->user->name}}</td>
              <td><a href="/order/{{$orden->id}}/edit" class="btn btn-primary" role="button">editar</a></td>
              <td><a href="/order/{{$orden->id}}/show" class="btn btn-primary" role="button">ver</a></td>
              @endcan

              @can('delete', $orden)
              <td>
              <form method="post" action="/order/{{ $orden->id }}" >
                  {{ csrf_field() }}
                  <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar" class="btn btn-primary">
                </form>
                </td>
              @endcan

            </tr>

            @empty
            <h1>no hay pedidos que mostrar</h1>
            @endforelse
          </table>
          {{ $order->render() }}

          <br>
        </div>

      </div>
    </div>
  </div>
</div>
</div>

@endsection
