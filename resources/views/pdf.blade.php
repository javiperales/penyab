<!DOCTYPE html>

<html>

<head>

  <title>Detalle de Pedido</title>
  <style type="text/css">

  body {
    font-family:Helvetica,Futura,Arial,Verdana,sans-serif;
  }

  h1{
    text-align: center;
  }
  h2{

    text-align: right;
  }
  h3{
    font-weight: normal;

  }

  .titulo{
    background-color:grey;

  }

  .nopagado{

    color:red;
  }

table {
  border-collapse: collapse;
  width: 100%;
}

th, td {
  text-align: right;
  padding: 8px;
}

tr:nth-child(even) {background-color: #829193;}


</style>
</head>

<body>

  <h1 class="titulo">Detalle del pedido: {{ $order->id }} de {{$order->user->name}}</h1>
  <hr>

  <h3>
    Estado:
    @if ($order->paid == 0)
    {{ "Sin pagar" }}
    <hr>
    @else
    {{ "Pagado" }}
    <hr>
    @endif
  </h3>

  <h3> Productos comprados: </h3>

  <table class="table">
    <hr>
    <tr>
      <th>id</th>
      <th>pagado</th>
      <th>fecha</th>
    </tr>
    <hr>
    <tr>
      <td>{{$order->id}}</td>
      <td>{{$order->paid}}</td>
      <td>{{$order->date->format('d-m-Y')}}</td>
    </tr>
    <tr>
      <td>nombre</td>
      <td>precio</td>
      <td>cantidad</td>
      <td>total</td>
    </tr>
    @foreach($order->products as $product)
    <tr>
      <td>{{$product->name}}</td>
      <td>{{$product->price}}</td>
      <td>{{$product->pivot->quantity}}</td>
      <td>{{$product->pivot->quantity*$product->price}}</td>
    </tr>

    @endforeach
<tr>
  <td colspan="3">total del pedido </td>
  <td>{{ $order->total($order->id) }}</td>
</tr>
  </table>


  @if ($order->paid == 1)
  @else
  <h2 class="nopagado"> Pendiente de pago.</h2>
  @endif

</body>

</html>
