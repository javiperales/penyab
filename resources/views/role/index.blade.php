@extends('layouts.app')

@section('content')
<h1>Lista de roles</h1>
<a href="/users/create">Nuevo</a>

<table class="table table-striped table-hover">
    <tr>
        <td>Nombre</td>
        <td>id</td>

    </tr>


    @forelse ($roles as $role)
    <tr>
        <td> {{ $role->name }} </td>
        <td> {{ $role->id }} </td>

        <td>



           @can('view', $role)
           <a href="/roles/{{$role->id}}" class="btn btn-primary">Ver</a>
           @endcan
       </td>

   </tr>

   @empty
   <li>No hay roles!!</li>
   @endforelse
</table>
{{ $roles->render() }}

@endsection
