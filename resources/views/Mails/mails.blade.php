<h2> ¡PEDIDO COMPLETADO {{$order->user->name}}! </h2>


<table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>Id</th>
        <th>Nombre</th>
        <th>Precio</th>
        <th>Categoría</th>
    </tr>
</thead>
<tbody>
  @forelse ($order->products as $product)
  <tr>
    <td>{{ $product->id }}</td>
    <td>{{ $product->name }}</td>
    <td>{{ $product->price }}</td>
    <td>{{ $product->cathegory->name }}</td>
    @empty
    @endforelse
</tr>
</tbody>
</table>
<p> Gracias por confiar en nosotras.</p>
