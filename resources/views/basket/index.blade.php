@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> <br>
                   <!--  {{-- <a href="/products/create" class="btn btn-primary">Nuevo</a></div> --}} -->

                   <div class="card-body">
                    <table class="table table-striped table-hover">
                        <thead>
                            <a href="/product"  class="btn btn-success btn-block active" role="button">añadir producto</a>
                            <tr>
                                @if($products!=null)
                                <h1>Lista de productos en cesta</h1>
                                <td>nombre</td>
                                {{--  <td>id</td> --}}
                                <td>cantidad</td>
                                <td>precio</td>
                                <td>importe</td>
                                @endif

                                @forelse($products as $producto)
                                <tr>
                                    <td>{{$producto->name}}</td>
                                    {{-- <td>{{$producto->id}}</td> --}}
                                    <td><a class="btn btn-default" href="/basket/{{ $producto->id }}">+</a>{{$producto->cantidad}}<a class="btn btn-default" href="/basket/delete/{{ $producto->id }}">-</a></td>
                                    <td>{{$producto->price}}</td>
                                    <td>{{$producto->importe}}</td>
                                    @endforeach

                                </table>

                                @if($products!=null)
                                <h4>importe total={{$total}}</h4>

                                <a href="/basket/flush" class="btn btn-success">vaciar cesta</a>

                               <form method="post" action="/basket">
                                    {{csrf_field()}}
                                    <input type="submit" name="" value="confirmar pedido"
                                    class="btn btn-success">


                               </form>
                                @elseif($products==null)
                                 <h1>no hay productos en la cesta</h1>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endsection
